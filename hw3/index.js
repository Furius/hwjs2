function Hamburger(size, stuffing) {
    if (size != "small"){
        if(size != "large"){
            throw new HamburgerException("Неверный размер")
        }
    }
    else{
        this.size = size;
    }
    if (stuffing != "cheese"){
        if(stuffing != "salad"){
            if (stuffing != "potato"){
                throw new HamburgerException("Неверно указана начинка")
            }
        }
    }
    else{
        this.stuffing = stuffing;
    }
} 
try{
    /* Размеры, виды начинок и добавок */
    const SIZE_SMALL = "small";
    const SIZE_LARGE = "large";
    const STUFFING_CHEESE = "cheese";
    const STUFFING_SALAD = "salad";
    const STUFFING_POTATO = "potato";
    const TOPPING_MAYO = "mayo";
    const TOPPING_SPICE = "spice";

    //Функция исключения
    function HamburgerException(message){
        if(message != undefined){
            console.log(message);
            return;
        }
    }

    /**
     * Добавить добавку к гамбургеру. Можно добавить несколько
     * добавок, при условии, что они разные.
     * 
     * @param topping     Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */


    Hamburger.prototype.addTopping = function (topping) {
        if(this.topping1 == topping || this.topping == topping){
            throw new HamburgerException("Нельзя добавить 2 одинаковых добавки")
        }
        if(topping == undefined){
            throw new HamburgerException("Вы не ввели ни одной добавки")
        }
        if(topping == "mayo"){
            this.topping = topping;
        }
        else if(topping == "spice"){
            this.topping1 = topping;
        }
        else if(topping != "mayo"){
            if(topping != undefined && topping != "spice"){
                throw new HamburgerException("Неверно указана добавка");
            }
        }
    }

    //Для удаления добавки
    Hamburger.prototype.removeTopping = function (topping) {
        if(this.topping == topping){
            delete this.topping;
        }
        else if(this.topping1 == topping){
            delete this.topping1;
        }
        else if(topping == undefined){
            throw new HamburgerException("Вы ничего не ввели")
        }
        else {
            throw new HamburgerException("Неверно введена добавка") 
        }
    }

    //Узнать количество добавок 
    Hamburger.prototype.getToppings = function (){
        let toparray = [];
        if(this.topping != undefined){
            toparray.push(this.topping);
        }
        if(this.topping1 != undefined){
            toparray.push(this.topping1);
        }
        else if(this.topping1 == undefined && this.topping == undefined){
            throw new HamburgerException("Вы удалили, или не ввели ни одной добавки")
        }
        return toparray;
    }

    //Узнать размер гамбургера
    Hamburger.prototype.getSize = function (){
        if(this.size != undefined){
            return  this.size;
        }
        else{
            throw new HamburgerException("Вы не выбрали размер гамбургера");
        }
    }

    //Узнать начинку 
    Hamburger.prototype.getStuffing = function(){
        if(this.stuffing != undefined){
            return this.stuffing;
        }
        else{
            throw new HamburgerException("Вы не добавили начинку");
        }
    }

    //Узнать цену
    Hamburger.prototype.calculatePrice = function (){
        let price = 0;
        if(this.size == "small"){
            price += 50;
        }
        else if(this.size == "large"){
            price += 100;
        }
        if(this.stuffing == "cheese"){
            price += 10;
        }
        else if(this.stuffing == "salad"){
            price += 20;
        }
        else if(this.stuffing == "potato"){
            price += 15;
        }
        if(this.topping == "mayo"){
            price += 20;
        }
        else if(this.topping == "spice"){
            price += 15;
        }
        if(this.topping1 == "mayo"){
            price += 20;
        }
        else if(this.topping1 == "spice"){
            price += 15;
        }
        return price;
    }

    //Узнать каллории
    Hamburger.prototype.calculateCalories = function (){
        let calories = 0;
        if(this.size == "small"){
            calories += 20;
        }
        else if(this.size == "large"){
            calories += 40;
        }
        if(this.stuffing == "cheese"){
            calories += 20;
        }
        else if(this.stuffing == "salad"){
            calories += 5;
        }
        else if(this.stuffing == "potato"){
            calories += 10;
        }
        if(this.topping == "mayo"){
            calories += 5;
        }
        else if(this.topping == "spice"){
            calories += 0;
        }
        if(this.topping1 == "mayo"){
            calories += 5;
        }
        else if(this.topping1 == "spice"){
            calories += 0;
        }
        return calories;
    }

/*
let humburger = new Hamburger(SIZE_SMALL, STUFFING_CHEESE)
humburger.addTopping("mayo", "spice");
humburger.getToppings();
humburger.getSize();
humburger.getStuffing();
humburger.calculatePrice();
humburger.calculateCalories();
*/

// маленький гамбургер с начинкой из сыра
var hamburger = new Hamburger(SIZE_SMALL, STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(TOPPING_SPICE);
// А сколько теперь стоит? 
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log("Is hamburger large: %s", hamburger.getSize() === SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(TOPPING_SPICE);
console.log("Have %d toppings", hamburger.getToppings().length); // 1
}
catch{
    alert("Error")
}