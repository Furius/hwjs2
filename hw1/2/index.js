function Human(age, height, weight, color){
    this.age = age;
    this.height = height;
    this.weight = weight;
    this.color = color;
    this.loveSport = function(sport){
        return "I love" + sport;
    }
}
Human.type = "Human";
Human.heartscamber = 4;
Human.eat = function(){
    return "Sounds of eating";
}
Human.walk = function(){
    return "Sounds of walking";
}
Human.breath = function(){
    return "Sounds of breathing"
}

const a = new Human(20, 70, 187, "black");
