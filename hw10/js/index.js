
//Переменные для работы
const createTable = document.createElement("table"),
    createTankRed = document.createElement("img"),
    createDivForTanks = document.createElement("div");
    createDivForTanks.setAttribute("id", "divForTanks");
    createTankRed.setAttribute("src", "img/tank.png");
    createTankRed.setAttribute("id", "red");
let countTd = 0,
    redTopBottom = 94,
    redLeftRight = 50,
    letPlay = true,
    letShot = true,
    shotVrag = true,
    marginBlack = 2,
    topRaketa,
    interval,
    intervalVrag,
    redLeftRaketa,
    topVragRaketa = marginBlack + 6,
    marginLeft = 4;

//Стили для танка красного
createTankRed.style.cssText = `
    position: absolute;
    transform:rotate(0deg);
    top: 94%;
    left: 50%;
    z-index: 1000;
    width: 44px;
    height: 44px;
`

//Сама таблица
document.body.append(createTable);
createTable.append(createDivForTanks);

//Рисуем поле 10 х 10
for(let i = 0; i < 50; i++){
    const createTr = document.createElement("tr");
    createTable.append(createTr);
    for(let j = 0; j < 50; j++){
        const createTd = document.createElement("td");
        createTd.setAttribute("id", `td${countTd}`);
        createTr.append(createTd);
        countTd++;
    };
};

//Добавляем танки
createDivForTanks.append(createTankRed);
for(let i = 0; i < 10; i++){
    const createTankBlack = document.createElement("img");
    createTankBlack.setAttribute("src", "img/tank1.png");
    createTankBlack.setAttribute("class", "black");
    createTankBlack.setAttribute("id", `black${i}`);
    //Стили для танка черного
    createTankBlack.style.cssText = `
    z-index: 1000;
    position:absolute;
    width: 44px;
    left: ${marginLeft}%;
    top: 2%;
    height: 44px;
    `
    marginLeft += 9.5;
    createTankRed.before(createTankBlack)
};
marginLeft = 4;

//Интервал который будет двигать танки вперед каждые 3 секунды
setInterval(() => {
    if(letPlay == true){
        marginBlack += 2
        const tanksBlack = document.getElementsByClassName("black");
        for(let i = 0; i < tanksBlack.length; i++){
            let tank = tanksBlack[i];
            tank.style.top = `${marginBlack}%`;
        }
        setTimeout(() => { //Каждые 2 передижения танков, они будут выпускать каждыйпо ракете 
            if(shotVrag == true){
                let countRaketa = 4;
                let counter = 10;
                let counteri = 0;
                let copyVragRaketa;
                const BlackRakets = document.getElementsByClassName("vragrak");
                while(BlackRakets.length < counter){ //Цикл для того чтобы каждый танк выпустил ракету
                    const vragRaketa = document.createElement("img");
                    copyVragRaketa = vragRaketa;
                    let tank = document.querySelector(`#black${counteri}`);
                    if(tank == null){
                        countRaketa += 9.5;
                        counteri++;
                        counter--;
                        continue;
                    }
                    vragRaketa.setAttribute("src", "img/raketavrag.png")
                    vragRaketa.setAttribute("class", `vragrak`);
                    vragRaketa.style.cssText = `
                        position: absolute;
                        top:${marginBlack + 6}%;
                        left:${countRaketa + 2.6}%;
                        height: 30px;
                        z-index: 1000;
                        `;
                    tank.after(vragRaketa);
                    countRaketa += 9.5;
                    counteri++;
                }
                const raketsVrag = document.getElementsByClassName("vragrak");
                intervalVrag = setInterval(() => { //Интервал для движения ракет вниз
                    for(let j = 0; j < raketsVrag.length; j++){
                        let raketavrag = raketsVrag[j];
                            if(raketavrag != undefined){
                                raketavrag.style.transition = "0.1s top";
                                if(raketavrag.style.top == "96%"){
                                    for(let i = 0; i < 10; i++){ //При достижении ракет края поля,они будут взрыватся
                                        if(raketavrag != undefined){
                                            const bum = document.createElement("img");
                                            bum.setAttribute("src", "img/bum.gif");
                                            bum.setAttribute("class", "bum");
                                            bum.style.cssText = `
                                                position: absolute;
                                                top:${raketavrag.style.top};
                                                left:${raketavrag.style.left};
                                                z-index: 1000;
                                                width: 44px;
                                                height: 44px;
                                            `;
                                            raketavrag = raketsVrag[0];
                                            if(raketavrag != undefined){
                                                raketavrag.replaceWith(bum);
                                            }
                                        }
                                    }
                                    setTimeout(() => { //Взрив каждой ракеты
                                        const bum = document.getElementsByClassName("bum");
                                        for(let i = 0; i < 10; i++){
                                            if(bum[0] != undefined){
                                                bum[0].remove();
                                            }
                                        }
                                        topVragRaketa = marginBlack + 6;
                                        shotVrag = true;
                                    }, 1500);  
                                    clearInterval(intervalVrag); 
                                }
                            }
                        if(raketavrag != undefined){
                            topVragRaketa += 2;
                            raketavrag.style.top = `${topVragRaketa}%`
                            topVragRaketa -= 2;
                        }
                    }
                    topVragRaketa += 1;
                }, 50);
                shotVrag = false;
            }
        }, 1000);
    }
}, 3000);

let mainInterval = setInterval(() => { //Основной интревал который проверяет на все исходы игры(есл ракета попала по красному танку - проигрышь, если ракета красного танка попала по черному - он взорвется, если мы попытаемся пройти за спину танков не убив всех противников - проигрышь и т.д)
    if(letPlay ==true){ //Если все танки уничтожены - победа
        if(createDivForTanks.childNodes.length == 1 && createDivForTanks.childNodes[0].id == "red"){
            setTimeout(() => {
                alert("You win! Reload the page to restart the game");
                clearInterval(mainInterval);
            }, 2000);
            const salut = document.createElement("img");
            salut.setAttribute("src", "img/salut.gif");
            salut.setAttribute("class", "bum");
            salut.style.cssText = `
            position: absolute;
            top:${redTopBottom - 5}%;
            left:${redLeftRight}%;
            z-index: 1000;
            width: 44px;
            height: 44px;
            `;
            createTankRed.after(salut);
            letPlay = false;
        }
        let margin = marginBlack + 6;
        if(margin == redTopBottom){ //Если мы зашли за спину врагов не уничтожив их, или же столкнулись с вражеским танком 
            setTimeout(() => {
                alert("You lose! You must destroy all enemies");
                clearInterval(mainInterval);
            }, 2000);
            const bum = document.createElement("img");
            bum.setAttribute("src", "img/bum.gif");
            bum.setAttribute("class", "bum");
            bum.style.cssText = `
            position: absolute;
            top:${redTopBottom}%;
            left:${redLeftRight}%;
            z-index: 1000;
            width: 44px;
            height: 44px;
            `
            createTankRed.replaceWith(bum)
            letPlay = false;
            setTimeout(() => {
                bum.remove();
            }, 1500);
        }

        let raketaTopForCheck = marginBlack + 4;
        if(raketaTopForCheck == topRaketa){ //Союзная ракета будет взрыватся при достижении края поля
            const checkRaketa = document.querySelector("#raketa")
            if(checkRaketa != null){
                for(let i = 0; i < 10; i++){
                    const checktank = document.querySelector(`#black${i}`)
                    if(checktank == null){
                        continue;
                    }
                    if(checktank.getBoundingClientRect().left < checkRaketa.getBoundingClientRect().left && checktank.getBoundingClientRect().right > checkRaketa.getBoundingClientRect().right){
                        const bum = document.createElement("img");
                        bum.setAttribute("src", "img/bum.gif");
                        bum.setAttribute("class", "bum");
                        bum.style.cssText = `
                        position: absolute;
                        top:${marginBlack}%;
                        left:${redLeftRaketa}%;
                        z-index: 1000;
                        width: 44px;
                        height: 44px;
                        `
                        checkRaketa.remove();
                        checktank.replaceWith(bum);
                        setTimeout(() => {
                            bum.remove();
                        }, 1500);
                        break;
                    }
                }
            }
        }

        let checkProbitie = `${redTopBottom - 2}%`;
        const raketi = document.getElementsByClassName("vragrak"),
            redTank = document.querySelector("#red");
        for(let i = 0; i < raketi.length; i++){ //Если по красному танку попала вражеская ракета - проигрышь
            if(raketi[i].style.top == checkProbitie){
                if(redTank.getBoundingClientRect().left < raketi[i].getBoundingClientRect().left && redTank.getBoundingClientRect().right > raketi[i].getBoundingClientRect().right){
                    setTimeout(() => {
                        alert("OOPS! You lose :)")
                    }, 3000);
                    letPlay = false;
                    const bum = document.createElement("img");
                    bum.setAttribute("src", "img/bum.gif");
                    bum.setAttribute("class", "bum");
                    bum.style.cssText = `
                    position: absolute;
                    top:${redTopBottom}%;
                    left:${redLeftRight}%;
                    z-index: 1000;
                    width: 44px;
                    height: 44px;
                    `
                    raketi[i].remove();
                    redTank.replaceWith(bum);
                    setTimeout(() => {
                        bum.remove();
                    }, 1500);
                    break;
                }
            }
        }
    }
}, 1);

document.addEventListener("keydown", function(event){ //Клавиши управления: w, a, d, s аналогичны стрелочкам (предусмотрена русская и украинская раскладка)
    if(letPlay == true){
        if(event.key == "w" || event.key == "ц"){
            if(redTopBottom == 2){
                return;
            }else{
                createTankRed.style.transform = "rotate(0deg)"
                createTankRed.style.top = `${redTopBottom -= 2}%`;
            }
        }
        if(event.key == "a" || event.key == "ф"){
            if(redLeftRight == 0){
                return;
            }else{
                createTankRed.style.transform = "rotate(-90deg)"
                createTankRed.style.left = `${redLeftRight -= 2}%`;
            };
        }
        if(event.key == "d" || event.key == "в"){
            if(redLeftRight == 94){
                return;
            }else{
                createTankRed.style.transform = "rotate(90deg)"
                createTankRed.style.left = `${redLeftRight += 2}%`;
            };
        }
        if(event.key == "s" || event.key == "ы" || event.key == "і"){
            if(redTopBottom == 94){
                return;
            }else{
                createTankRed.style.transform = "rotate(180deg)"
                createTankRed.style.top = `${redTopBottom += 2}%`;
            };
        };
    }
})

document.addEventListener("keydown", function(event){ //При нажатии на пробел будет выпущена ракета
    if(letPlay == true){
        if(letShot == true){
            const tank = document.querySelector("#red");
            if(event.code == "Space" && tank.style.transform == "rotate(0deg)"){
                redLeftRaketa = redLeftRight - 0.5;
                const raketa = document.createElement("img");
                raketa.setAttribute("src", "img/raketa.png");
                raketa.setAttribute("id", "raketa");
                raketa.style.cssText = `
                position: absolute;
                top:${redTopBottom - 4}%;
                left:${redLeftRight + 2.5}%;
                height: 30px;
                z-index: 1000;
                `
                createTankRed.after(raketa);
                topRaketa = redTopBottom - 4;
                raketa.style.transition = "0.1s top";
                interval = setInterval(() => {
                    if(topRaketa == 0){
                        const bum = document.createElement("img");
                        bum.setAttribute("src", "img/bum.gif");
                        bum.setAttribute("class", "bum");
                        bum.style.cssText = `
                            position: absolute;
                            top:${topRaketa}%;
                            left:${redLeftRaketa}%;
                            z-index: 1000;
                            width: 44px;
                            height: 44px;
                        `;
                        raketa.replaceWith(bum);
                        setTimeout(() => {
                            bum.remove();
                            letShot = true;
                            topRaketa = redTopBottom - 4;
                        }, 1500);  
                        clearInterval(interval);       
                    }
                    topRaketa -= 1;
                    raketa.style.top = `${topRaketa}%`
                }, 50);
                letShot = false;
            }
        }
    }
})