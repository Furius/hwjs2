let createimg;
let checkLose = false;

//Рандом для того чтобы бомбочки расставлялись в рандомном порядке(хаотично), а так же добавление бомб на поле
function random(){
    for(let i = 0; i < 10; i++){
        let rand = Math.random();
        let rand1 =  rand * 64;
        let rand2 = Math.floor(rand1);
        let getByRand = document.querySelector(`#div${rand2}`)
        if (getByRand.childNodes.length > 0){
            i--;
            continue;
        }
        createimg = document.createElement("img");
        createimg.setAttribute("src", "mina.jpg");
        createimg.style.display = "none";
        getByRand.append(createimg);   
    }
}

//Создание самого поля
let arrid = [];
let getosnova = document.querySelector("#getosn");
for(let i = 0; i < 64; i++){
    let creatediv = document.createElement("div");
    creatediv.setAttribute(`id`, `div${i}`);
    getosnova.append(creatediv);
    arrid.push[i];
}

//Вызов функции с рандомом бомбочек
random();

//Переменные для дальнейшего их использования
let getCountMin = 0;
let countflag = 0;
let getButtondiv = document.querySelector("#divrest");
let getButton = document.querySelector("#restartgame");
let getPOfMin = document.querySelector("#countmin");
let getPOfFlag = document.querySelector("#countflag");
let getchildosn = getosnova.childNodes;
let flaglet = false;
let exampleParag = document.createElement("p");

//Кнопка рестарта
getButton.onclick = function(){
    checkLose = false;
    getchildosn.forEach(element => {
        if(element.nodeType != 3){
            let delelement = element.firstChild;
            if(delelement != null){
                delelement.remove();
            }
            //Если стоит флажок, мы его очищаем
            if(element.style.backgroundImage = "url(flag.png)"){
                element.style.backgroundImage = "";
            }
        }
    });
    //Очищаем все данные, а так же счетчик бомб и флажков
    countflag = 0;
    getCountMin = 0;
    flaglet = false;
    getPOfMin.innerHTML = `Найденых мин: ${getCountMin} / 10`;
    getPOfFlag.innerHTML = `Поставлено флажков: ${countflag} / 10`;
    random();
}

//Клик по ячейке
getosnova.onclick = function(event){
    if(checkLose == false){
        //Задаем текст счетчиков
        if(getButtondiv.style.display == ""){
            getPOfFlag.innerHTML = `Поставлено флажков: 0 / 10`;
            getPOfMin.innerHTML = `Найденых мин: 0 / 10`;
        }
        flaglet = true;
        getButtondiv.style.display = "flex";
        let target = event.target;
        //Если под флажком находитья цифра, ничего не произойдет(это исключено, потому что в дальнейшем коде флажок нельзя ставить на цифру, а если поставить цифру на флажок, он пропадет. Этот код существует для того чтобы он сработал вслучае возникновения ошибки из-за возможных правок в коде)
        if(target.style.backgroundImage == `url("flag.png")`){
            if(target.firstChild != null){
                if(target.firstChild.tagName != createimg.tagName){
                    return;    
                }
            }
        }
        //Если стоит флажок,  при клике мы его уберем и напишем вместо него цифру, если первый элемент ячейки это параграф, если же картинка, мы проиграем
        if(countflag > 0){
            if(target.style.backgroundImage != ""){
                target.style.backgroundImage = "";
                countflag--;
                getPOfFlag.innerHTML = `Поставлено флажков: ${countflag} / 10`;
            }
        }
        let getDivNum;
        let getCreateNum;
        let getTop;
        let getBot;
        let getTopRight;
        let getTopLeft;
        let getBotRight;
        let getBotLeft;
        let checkAlert = false;
        let minusmin = false;

        //Код проигрыша
        if(target.childNodes.length > 0){
            if(target.firstChild.tagName == createimg.tagName){
                target.childNodes[0].style.display = "block";
                setTimeout(() => {
                    alert("YOU LOSE!");
                }, 0);
                checkAlert = true;
            }
        }

        //Показываем все мины после проигрыша
        if(checkAlert == true){
            getchildosn.forEach(element => {
                if(element.nodeType !=3 && element.firstChild != null){
                    if(element.firstChild.tagName == createimg.tagName){
                        element.firstChild.style.display = "";
                        checkLose = true;
                    }
                }
            });
        }

        //Если на ячейке ничего нету рисуем цифру
        if(target.childNodes.length == 0){
            function checkNum(targ){
                let countmin = 0;
                let createNum = document.createElement("p");
                createNum.style.cssText = `
                text-align:center;
                font-size:20px;
                font-weight:bold;
                `;
                getCreateNum = createNum;
                let prev = targ.previousSibling;
                let next = targ.nextSibling;
                let divid = targ.id;
                let divnum = divid.substr(3,3);
                if(targ == target){
                    getDivNum = divnum;
                }
                let topnum = +divnum - 8;
                let top = document.querySelector(`#div${topnum}`);
                let botnum = +divnum + 8;
                let bot = document.querySelector(`#div${botnum}`);
                let topleftnum = +divnum - 9;
                let topleft = document.querySelector(`#div${topleftnum}`)
                let toprightnum = +divnum - 7;
                let topright = document.querySelector(`#div${toprightnum}`)
                let bottomleftnum = +divnum + 7;
                let bottomleft = document.querySelector(`#div${bottomleftnum}`)
                let bottomrightnum = +divnum +9;
                let bottomright = document.querySelector(`#div${bottomrightnum}`)
                if(targ == target){
                    getTop = top;
                    getBot = bot;
                    getTopLeft = topleft;
                    getTopRight = topright
                    getBotLeft = bottomleft
                    getBotRight = bottomright;
                }

                //Проверяем сколько бомб находиться рядом с данной ячейкой, и записываем это количество в эту ячейку
                if(top != null){
                    if(top.firstChild != null){
                        if(top.firstChild.tagName == createimg.tagName){
                            countmin++;
                        }
                    }
                }
                if(bot != null){
                    if(bot.firstChild != null){
                        if(bot.firstChild.tagName == createimg.tagName){
                            countmin++;
                        }
                    }
                }
                if(prev != null && divnum != 0 && divnum != 8 && divnum != 16 && divnum != 24 && divnum != 32 && divnum != 40 && divnum != 48 && divnum != 56){
                    if(prev.firstChild != null){
                        if(prev.firstChild.tagName == createimg.tagName){
                            countmin++;
                        }
                    }
                }
                if(next != null && divnum != 7 && divnum != 15 && divnum != 23 && divnum != 31 && divnum != 39 && divnum != 47 && divnum != 55 && divnum != 63){
                    if(next.firstChild != null){
                        if(next.firstChild.tagName == createimg.tagName){
                            countmin++;
                        }
                    }
                }
                if(topleft != null && divnum != 0 && divnum != 8 && divnum != 16 && divnum != 24 && divnum != 32 && divnum != 40 && divnum != 48 && divnum != 56){
                    if(topleft.firstChild != null){
                        if(topleft.firstChild.tagName == createimg.tagName){
                            countmin++;
                        }
                    }
                }
                if(topright != null && divnum != 7 && divnum != 15 && divnum != 23 && divnum != 31 && divnum != 39 && divnum != 47 && divnum != 55 && divnum != 63){
                    if(topright.firstChild != null){
                        if(topright.firstChild.tagName == createimg.tagName){
                            countmin++;
                        }
                    }
                }
                if(bottomleft != null && divnum != 0 && divnum != 8 && divnum != 16 && divnum != 24 && divnum != 32 && divnum != 40 && divnum != 48 && divnum != 56){
                    if(bottomleft.firstChild != null){
                        if(bottomleft.firstChild.tagName == createimg.tagName){
                            countmin++;
                        }
                    }
                }
                if(bottomright != null && divnum != 7 && divnum != 15 && divnum != 23 && divnum != 31 && divnum != 39 && divnum != 47 && divnum != 55 && divnum != 63){
                    if(bottomright.firstChild != null){
                        if(bottomright.firstChild.tagName == createimg.tagName){
                            countmin++;
                        }
                    }
                }
                return countmin;
            }
            let funcNum = checkNum(target);
            //Записываем число в ячейку
            if (funcNum > 0){ 
                getCreateNum.innerHTML = funcNum;
                target.append(getCreateNum);
                countmin = 0;
            }

            //Если рядом с ячекой нету ни одной мины, то мы проверяем все соседние ячейки на наличие бомб в их соседних ячейках(вокруг ячейки с цифрой 0 все соседние ячейки покажут цифры которые будут обозначать количество мин находящихся рядом с этими ячейками)
            else{
                getCreateNum.innerHTML = funcNum;
                target.append(getCreateNum);
                if(target.previousSibling != null && target.previousSibling.style.backgroundImage != `url("flag.png")`){
                    if(target.previousSibling.firstChild == null && getDivNum != 0 && getDivNum != 8 && getDivNum != 16 && getDivNum != 24 && getDivNum != 32 && getDivNum != 40 && getDivNum != 48 && getDivNum != 56){
                        let funcForPrev = checkNum(target.previousSibling);
                        let createPForPrev = document.createElement("p");
                        createPForPrev.style.cssText = `
                        text-align:center;
                        font-size:20px;
                        font-weight:bold;
                        `;
                        createPForPrev.innerHTML = funcForPrev;
                        target.previousSibling.append(createPForPrev);
                    }
                }
                if(target.nextSibling != null && target.nextSibling.style.backgroundImage != `url("flag.png")`){
                    if(target.nextSibling.firstChild == null && getDivNum != 7 && getDivNum != 15 && getDivNum != 23 && getDivNum != 31 && getDivNum != 39 && getDivNum != 47 && getDivNum != 55 && getDivNum != 63){
                        let funcForNext = checkNum(target.nextSibling);
                        let createPForNext = document.createElement("p");
                        createPForNext.style.cssText = `
                        text-align:center;
                        font-size:20px;
                        font-weight:bold;
                        `;
                        createPForNext.innerHTML = funcForNext;
                        target.nextSibling.append(createPForNext);
                    }
                }
                if(getTop != null && getTop.style.backgroundImage == ""){
                    if(getTop.firstChild == null){
                        let funcForTop = checkNum(getTop);
                        let createPForTop = document.createElement("p");
                        createPForTop.style.cssText = `
                        text-align:center;
                        font-size:20px;
                        font-weight:bold;
                        `;
                        createPForTop.innerHTML = funcForTop;
                        getTop.append(createPForTop);
                    }
                }
                if(getTopLeft != null && getTopLeft.style.backgroundImage == ""){
                    if(getTopLeft.firstChild == null && getDivNum != 0 && getDivNum != 8 && getDivNum != 16 && getDivNum != 24 && getDivNum != 32 && getDivNum != 40 && getDivNum != 48 && getDivNum != 56){
                        let funcForTopLeft = checkNum(getTopLeft);
                        let createPForTopLeft = document.createElement("p");
                        createPForTopLeft.style.cssText = `
                        text-align:center;
                        font-size:20px;
                        font-weight:bold;
                        `;
                        createPForTopLeft.innerHTML = funcForTopLeft;
                        getTopLeft.append(createPForTopLeft);
                    }
                }
                if(getTopRight != null && getTopRight.style.backgroundImage == ""){
                    if(getTopRight.firstChild == null && getDivNum != 7 && getDivNum != 15 && getDivNum != 23 && getDivNum != 31 && getDivNum != 39 && getDivNum != 47 && getDivNum != 55 && getDivNum != 63){
                        let funcForTopRight = checkNum(getTopRight);
                        let createPForTopRight = document.createElement("p");
                        createPForTopRight.style.cssText = `
                        text-align:center;
                        font-size:20px;
                        font-weight:bold;
                        `;
                        createPForTopRight.innerHTML = funcForTopRight;
                        getTopRight.append(createPForTopRight);
                    }
                }
                if(getBot != null && getBot.style.backgroundImage == ""){
                    if(getBot.firstChild == null){
                        let funcForBot = checkNum(getBot);
                        let createPForBot = document.createElement("p");
                        createPForBot.style.cssText = `
                        text-align:center;
                        font-size:20px;
                        font-weight:bold;
                        `;
                        createPForBot.innerHTML = funcForBot;
                        getBot.append(createPForBot);
                    }
                }
                if(getBotLeft !=null && getBotLeft.style.backgroundImage == ""){
                    if(getBotLeft.firstChild == null && getDivNum != 0 && getDivNum != 8 && getDivNum != 16 && getDivNum != 24 && getDivNum != 32 && getDivNum != 40 && getDivNum != 48 && getDivNum != 56){
                        let funcForBotLeft = checkNum(getBotLeft);
                        let createPForBotLeft = document.createElement("p");
                        createPForBotLeft.style.cssText = `
                        text-align:center;
                        font-size:20px;
                        font-weight:bold;
                        `;
                        createPForBotLeft.innerHTML = funcForBotLeft;
                        getBotLeft.append(createPForBotLeft);
                    }
                }
                if(getBotRight != null && getBotRight.style.backgroundImage == ""){
                    if(getBotRight.firstChild == null && getDivNum != 7 && getDivNum != 15 && getDivNum != 23 && getDivNum != 31 && getDivNum != 39 && getDivNum != 47 && getDivNum != 55 && getDivNum != 63){
                        let funcForBotRight = checkNum(getBotRight);
                        let createPForBotRight = document.createElement("p");
                        createPForBotRight.style.cssText = `
                        text-align:center;
                        font-size:20px;
                        font-weight:bold;
                        `;
                        createPForBotRight.innerHTML = funcForBotRight;
                        getBotRight.append(createPForBotRight);
                    }
                }
            }
        }
    }
}
let flagDiv = undefined;

//Код для флажков, при нажатии правой кнопки мыши
getosnova.addEventListener("contextmenu", function(event){
    if(checkLose == false){ //Если мы еще не проиграли
        if(flaglet == true){ //Если разрешено ставить флажки
            flagDiv = event.target;
            //Проверка на наличие мины в этой ячейке
            if(flagDiv.tagName == getosnova.tagName && flagDiv.style.backgroundImage == ""){
                if(flagDiv.firstChild != null){
                    //Если в ячейке не мина, а параграф, флажок не поставится 
                    if(flagDiv.firstChild.tagName == exampleParag.tagName){
                        return;
                    }
                    //Если в ячейке найдено мину, счетчик мин увеличится на 1
                    if(flagDiv.firstChild.tagName == createimg.tagName){
                        getCountMin++;
                        getPOfMin.innerHTML = `Найденых мин: ${getCountMin} / 10`;
                        minusmin = true;
                    }
                }
                //Увеличиваем счетчик флажков с каждым новым флажком
                if(countflag < 10){
                    countflag++;
                    getPOfFlag.innerHTML = `Поставлено флажков: ${countflag} / 10`;
                }
                //Если мы поставили 10 флажков
                if(countflag >= 10){
                    flagDiv.style.backgroundImage = `url(flag.png)`;
                    setTimeout(() => {
                        alert("Вы раставили все флажки");
                    }, 0);
                    flaglet = false;
                }
            }
            //Если мы пытаемся поставить флажок на цифру, ничего не произойдет
            if(flagDiv.tagName == exampleParag.tagName){
                return;
            }
            //Ставим флажок
            if(flagDiv.style.backgroundImage == "" && flaglet != false){
                flagDiv.style.backgroundImage = `url(flag.png)`;
            }
            //Если флажок стоит, убераем его и минусуем счетчик флажков
            else if(flagDiv.style.backgroundImage == `url("flag.png")` && flaglet != false){
                flagDiv.style.backgroundImage = "";
                if(countflag > 0){
                    countflag--;
                    getPOfFlag.innerHTML = `Поставлено флажков: ${countflag} / 10`;
                }
                if(getCountMin > 0 && flagDiv.firstChild != null){
                    if(flagDiv.firstChild.tagName == createimg.tagName){
                        getCountMin--;
                        getPOfMin.innerHTML = `Найденых мин: ${getCountMin} / 10`;
                        minusmin = false;
                    }
                }
            }
            //Если мы расставили все флажки над всеми минами, мы выигрываем и показывает все мины 
            if(getPOfMin.textContent == "Найденых мин: 10 / 10"){
                getchildosn.forEach(element => {
                    if(element.nodeType !=3 && element.firstChild != null){
                        if(element.firstChild.tagName == createimg.tagName){
                            element.firstChild.style.display = "";
                        }
                    }
                });
                setTimeout(() => {
                    alert("YOU WIN!")
                }, 0);
                checkLose == true;
            }
        }
        //Так же код удаления флажка
        else if(flagDiv != undefined){
            flagDiv = event.target;
            if(flagDiv.style.backgroundImage == `url("flag.png")`){
                flagDiv.style.backgroundImage = "";
                if(countflag > 0){
                    countflag--;
                    getPOfFlag.innerHTML = `Поставлено флажков: ${countflag} / 10`;
                }
                if(getCountMin > 0 && flagDiv.firstChild != null){
                    getCountMin--;
                    getPOfMin.innerHTML = `Найденых мин: ${getCountMin} / 10`;
                    minusmin = false;
                }
                flaglet = true;
            }
        }
    }
    
})
