let but = document.querySelector("#but");
let createInp = document.createElement("input");
let createParentDiv = document.createElement("div");

function randomForColor(){
    let rand = Math.random();
    let rand1 =  rand * 255;
    let rand2 = Math.floor(rand1);
    return rand2;
}
but.onclick = () => {
    createInp.setAttribute("placeholder", "Радиус");
    document.body.append(createInp);
}
let createBut = document.createElement("button");
let valInp;
createInp.onclick = () => {
    createInp.setAttribute("type", "number");
    if(document.querySelector("#but2") == null){
        createBut.setAttribute("id", "but2");
        createBut.innerHTML = "Нарисовать";
        document.body.append(createBut);
    }
}
let checkCreateDiv = true;
createBut.onclick = () => {
    valInp = createInp.value;
    createBut.after(createParentDiv);
    if(checkCreateDiv == true){
        for(let i = 0; i < 100; i++){
            let createKrug = document.createElement("div");
            let randColor = randomForColor();
            let randColor1 = randomForColor();
            let randColor2 = randomForColor();
            createKrug.style.cssText = `
            display:inline-block;
            border: 2px solid rgba(${randColor}, ${randColor1}, ${randColor2}); 
            border-radius: ${valInp}px; 
            width: ${valInp}px; 
            height: ${valInp}px;
            `;
            createParentDiv.append(createKrug);
        } 
        checkCreateDiv = false;
    }
}

createParentDiv.onclick = (event) => {
    let target = event.target;
    target.remove();
}