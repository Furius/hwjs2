const xhr = new XMLHttpRequest();
let arr = []; //Для выбраных элементов
xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
xhr.onreadystatechange = () => {
    if(xhr.status == 200 && xhr.readyState == 4){
        let a = JSON.parse(xhr.responseText);
        a.forEach(element => {
            if(element.rate > 25){
                arr.push(element);
            }
        });
        arr.forEach(element => {
            let txt = element.txt;
            let buy = element.rate;
            const div = document.querySelector(".buy");
            const createP = document.createElement("p");
            createP.setAttribute("class", "evro");
            createP.innerHTML = `${txt} - ${buy}`;
            div.append(createP);
        });
    }
    else if(xhr.status > 400 || xhr.status > 500){
        const divCurs = document.querySelector(".curse");
        divCurs.style.display = "none";
        const createError = document.createElement("p");
        createError.style.cssText = `
        text-align:center;
        font-size:3em;
        font-weight:bold;
        `;
        createError.innerHTML = "ОШИБКА";
        document.body.append(createError);
    }
}
xhr.send();
