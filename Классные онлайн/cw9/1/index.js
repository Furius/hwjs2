const xhr = new XMLHttpRequest(),
//Находим дивы
buyDiv = document.querySelector(".buy"),
curDiv = document.querySelector(".cur"),
sellDiv = document.querySelector(".sell");

//Задаем параметры запроса
xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");

//Когда обрабатываеться запрос
xhr.onreadystatechange = () => {
    if(xhr.status == 200 && xhr.readyState == 4){
        let a = JSON.parse(xhr.responseText);
        a.forEach(element => {
            let buy = element.rate;
            let cur = element.txt;
            if(cur == "СПЗ (спеціальні права запозичення)"){
                cur = "СПЗ" //Уменьшили чтобы не ломать таблицу
            }
            const pcur = document.createElement("p")
            pcur.setAttribute("class", "pcur");
            pcur.innerHTML = buy;
            buyDiv.append(pcur);
            const curp = document.createElement("p");
            curp.innerHTML = cur;
            curDiv.append(curp);
        });
    }
}
xhr.send();