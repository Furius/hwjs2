const mainDiv = document.querySelector("#mainDiv");

for (let i = 0; i < 10; i++) {
    const divs = document.createElement("div");
    divs.setAttribute("class", `div${i}`);
    mainDiv.append(divs);
    const createGif = document.createElement("img");
    createGif.setAttribute("src", "load.gif");
    createGif.setAttribute("id", `gif${i}`);
    createGif.style.cssText = `
    width:50px;
    height:50px;
    `;
    divs.append(createGif);
};

let url = "https://swapi.co/api/planets/";
let arrForNames = [],
    arrClimat = [],
    arrTerrain = [],
    arrResidents = [],
    arrForPlanetResidents = [],
    arrOfNameResidents = [];

let i = 0;


let promise = fetch(url);

promise.then(response => response.json())
    .then(data => {
        let arrPlanet = data.results;
        arrPlanet.forEach(element => {
            arrForNames.push(element.name);
            if (element.residents.length > 0) {
                element.residents.forEach(elem => {
                    arrForPlanetResidents.push(elem);
                });
                arrResidents.push(arrForPlanetResidents);
                arrForPlanetResidents = [];
            } else {
                arrResidents.push(arrForPlanetResidents);
                arrForPlanetResidents = [];
            };
            arrClimat.push(element.climate);
            arrTerrain.push(element.terrain);
        });
    })
    .then(function () {
        mainDiv.childNodes.forEach(element => {
            element.append(arrForNames[i]);
            const findImg = document.querySelector(`#gif${i}`);
            if (findImg != undefined) {
                findImg.remove();
            }
            i++;
        });
        i = 0;
    })
    .then(function () {
        arrResidents.forEach(element => {
            if (element.length > 0) {
                let arrElement = arrResidents.indexOf(element);
                const createH4 = document.createElement("h4");
                createH4.innerHTML = "Родина для:";
                mainDiv.childNodes[arrElement].append(createH4)
                const createGif = document.createElement("img");
                createGif.setAttribute("src", "load.gif");
                createGif.setAttribute("id", `gif${arrElement}`);
                createGif.style.cssText = `
                width:50px;
                height:50px;
                `
                mainDiv.childNodes[arrElement].append(createGif);
            }
        });

        arrResidents.forEach(element => {
            let arrElement = element;
            if (element.length > 0) {
                element.forEach(elem => {
                    let indexOfElement = arrResidents.indexOf(arrElement);
                    let promise1 = fetch(elem);
                    promise1.then(response => response.json())
                        .then(data => {
                            const pForDiv = document.createElement("p");
                            pForDiv.innerHTML = data.name;
                            pForDiv.setAttribute("id", "pafter");
                            mainDiv.childNodes[indexOfElement].firstElementChild.after(pForDiv);
                            const findImg = document.querySelector(`#gif${indexOfElement}`);
                            if (findImg != undefined) {
                                findImg.remove();
                            }
                        })

                });
            }
        });
    })
    .then(function () {
        let i = 0;
        arrClimat.forEach(element => {
            const createH4 = document.createElement("h4");
            createH4.innerHTML = "Климат:";
            mainDiv.childNodes[i].append(createH4);
            i++;
        });
        i = 0;
        mainDiv.childNodes.forEach(element => {
            const pForDiv = document.createElement("p");
            pForDiv.innerHTML = arrClimat[i];
            element.append(pForDiv);
            i++;
        });
        i = 0;
    })
    .then(function () {
        let i = 0;
        arrTerrain.forEach(element => {
            const createH4 = document.createElement("h4");
            createH4.innerHTML = "Местность:";
            mainDiv.childNodes[i].append(createH4);
            i++;
        });
        i = 0;
        mainDiv.childNodes.forEach(element => {
            const pForDiv = document.createElement("p");
            pForDiv.innerHTML = arrTerrain[i];
            element.append(pForDiv);
            i++;
        });
        i = 0;
    })
