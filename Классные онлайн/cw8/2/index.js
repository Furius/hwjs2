const select = document.getElementsByName("color"),
button = document.querySelector("#but"),
number = document.querySelector("#num");

if(localStorage.bgColor != undefined){
    select[0].value = localStorage.bgColor;
    document.body.style.backgroundColor = localStorage.bgColor;
}
if(localStorage.fontSize != undefined){
    number.value = localStorage.fontSize;
    document.body.style.fontSize = localStorage.fontSize;
}


button.addEventListener("click", function(){
    document.body.style.backgroundColor = select[0].value;
    localStorage.bgColor = select[0].value;
})

number.addEventListener("input", function(){
    document.body.style.fontSize = +number.value + "px";
    localStorage.fontSize = +number.value + "px";
})