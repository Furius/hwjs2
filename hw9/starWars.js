
//Переменные для работы
const xhr = new XMLHttpRequest(),
mainDiv = document.querySelector("#mainDiv");
let countDiv = 0,
APIhomeworld = [],
letXHR1 = false;

//Функция которая будет создавать новый айакс запрос и вставлять названия кораблей в блок
function star(elem, parent){
    const xhr3 = new XMLHttpRequest();

    xhr3.open("GET", elem);
    xhr3.onreadystatechange = () => {
        let gif = document.querySelector(`#gif${parent.id}`);
        if (xhr3.status == 200 && xhr3.readyState == 4) {
            if(gif != null){
                gif.remove();
            };
            let parse = JSON.parse(xhr3.responseText);
            const shipsP = document.createElement("p");
            shipsP.innerHTML = parse.name;
            parent.append(shipsP);
        };
    }
    xhr3.send();
}

//Функция которая будет создавать новый айакс запрос и вставлять название родного мира персонажа под него
function ajax(element){
    const xhr1 = new XMLHttpRequest();

    xhr1.open("GET", element);
    xhr1.onreadystatechange = () => {
        const div = document.querySelector(`.div${countDiv}`),
        img = document.querySelector(`#img${countDiv}`),
        but = document.querySelector(`#but${countDiv}`);
        if (xhr1.status == 200 && xhr1.readyState == 4) {
            img.remove();
            let parse1 = JSON.parse(xhr1.responseText);
            let namePlanet = parse1.name;
            const homeworld = document.createElement("p");
            homeworld.innerHTML = namePlanet;
            if(div.contains(but)){
                but.before(homeworld);
            }else{
                div.append(homeworld);
            }
            countDiv++;
        }
    }
    xhr1.send();
    //Если я правильно понял, то после вызова функции ajax, эта функция каждый раз отправляла на сервер запрос с определенным url, который передавался в качестве аргумента, но без выполнения функции onreadystatechange, а после того как завершился так званый цикл и функция ajax была вызвана в последний раз, то в ней выполнился метод send и после него выполнился метод onreadystatechange, так как "цикл" был завершен. Метод onreadystatechange вызывается столько раз, сколько была вызвана функция, и каждый такой раз метод вызывается с тем url, который передавался сначала и до конца. На сколько я понял, то при отправке запроса на сервер, если эта отправка зациклена, то метод onreadystatechange будет выполнен в конце цикла и будет вызван на каждом url который был "запомнен" запросом.
}

//Создаем 10 дивов для 10 персонажей которые есть в API
for (let i = 0; i < 10; i++) {
    const divs = document.createElement("div");
    divs.setAttribute("class", `div${i}`);
    mainDiv.append(divs);
}

//Создаем запрос который будет первый обращатся к API со всеми персонажами и будет выводить их имя, гендер, а так же вызыват функцию которая сохздаст новый запрос и примет с него ответ с родным миром персонажа, а так же создаст кнопку "список кораблей" если персонаж управлял хотя-бы одним
xhr.open("GET", "https://swapi.co/api/people/");

xhr.onreadystatechange = () => {
    if (xhr.status == 200 && xhr.readyState == 4) {
        let parse = JSON.parse(xhr.responseText);
        parse.results.forEach(element => {
            const div = document.querySelector(`.div${countDiv}`),
                name = document.createElement("p"),
                img = document.createElement("img"),
                gender = document.createElement("p");

            name.innerHTML = element.name;
            gender.innerHTML = element.gender;
            APIhomeworld.push(element.homeworld);

            img.setAttribute("src", "load.gif");
            img.setAttribute("id", `img${countDiv}`);
            img.style.cssText = `
            height:50px;
            width:50px;
            `

            div.append(name);
            div.append(gender);
            div.append(img);
            if(element.starships.length > 0){
                const button = document.createElement("input");
                button.setAttribute("type", "button");
                button.setAttribute("value", "Список кораблей");
                button.setAttribute("id", `but${countDiv}`);
                div.append(button);
            }
            countDiv++;
        });
        for(let i = 0; i < 10; i++){
            ajax(APIhomeworld[i]);
        }
        countDiv = 0;
    }
}
xhr.send();

//При клике на кнопку "Список кораблей" будет заменена конопка на заголовок "пилотируемые корабли" и произойдет обращение к функции которая будет доставать из API список кораблей персонажа и выводить их. 
document.addEventListener("click", function(event){
    let target = event.target;
    if(target.tagName == "INPUT"){
        let parent = target.parentNode;
        let nameOfPers = parent.firstElementChild.textContent;
        parent.style.height = "250px";
        if(nameOfPers == "Luke Skywalker"){
            const xhr2 = new XMLHttpRequest();

            xhr2.open("GET", "https://swapi.co/api/people/");
            xhr2.onreadystatechange = () => {
                if (xhr2.status == 200 && xhr2.readyState == 4) {
                    let parse = JSON.parse(xhr2.responseText);
                    let starshipList = parse.results[0].starships;
                    starshipList.forEach(element => {
                        star(element, parent);
                        const rep = document.createElement("h3"),
                        gif = document.createElement("img");
                        gif.setAttribute("src", "load.gif");
                        gif.setAttribute("id", `gif${parent.id}`);
                        gif.style.cssText = `
                        height:50px;
                        width:50px;
                        `
                        rep.innerHTML = "Пилотируемые корабли:"
                        target.replaceWith(rep);
                        rep.after(gif);
                    });
                }
            }
            xhr2.send();
        }
        if(nameOfPers == "Darth Vader"){
            const xhr2 = new XMLHttpRequest();

            xhr2.open("GET", "https://swapi.co/api/people/");
            xhr2.onreadystatechange = () => {
                if (xhr2.status == 200 && xhr2.readyState == 4) {
                    let parse = JSON.parse(xhr2.responseText);
                    let starshipList = parse.results[3].starships;
                    starshipList.forEach(element => {
                        star(element, parent);
                        const rep = document.createElement("h3"),
                        gif = document.createElement("img");
                        gif.setAttribute("src", "load.gif");
                        gif.setAttribute("id", `gif${parent.id}`);
                        gif.style.cssText = `
                        height:50px;
                        width:50px;
                        `
                        rep.innerHTML = "Пилотируемые корабли:"
                        target.replaceWith(rep);
                        rep.after(gif);
                    });
                }
            }
            xhr2.send();
        }
        if(nameOfPers == "Biggs Darklighter"){
            const xhr2 = new XMLHttpRequest();

            xhr2.open("GET", "https://swapi.co/api/people/");
            xhr2.onreadystatechange = () => {
                if (xhr2.status == 200 && xhr2.readyState == 4) {
                    let parse = JSON.parse(xhr2.responseText);
                    let starshipList = parse.results[8].starships;
                    starshipList.forEach(element => {
                        star(element, parent);
                        const rep = document.createElement("h3"),
                        gif = document.createElement("img");
                        gif.setAttribute("src", "load.gif");
                        gif.setAttribute("id", `gif${parent.id}`);
                        gif.style.cssText = `
                        height:50px;
                        width:50px;
                        `
                        rep.innerHTML = "Пилотируемые корабли:"
                        target.replaceWith(rep);
                        rep.after(gif);
                    });
                }
            }
            xhr2.send();
        }
        if(nameOfPers == "Obi-Wan Kenobi"){
            const xhr2 = new XMLHttpRequest();
            parent.style.height = "400px"

            xhr2.open("GET", "https://swapi.co/api/people/");
            xhr2.onreadystatechange = () => {
                if (xhr2.status == 200 && xhr2.readyState == 4) {
                    let parse = JSON.parse(xhr2.responseText);
                    let starshipList = parse.results[9].starships;
                    starshipList.forEach(element => {
                        star(element, parent);
                        const rep = document.createElement("h3"),
                        gif = document.createElement("img");
                        gif.setAttribute("src", "load.gif");
                        gif.setAttribute("id", `gif${parent.id}`);
                        gif.style.cssText = `
                        height:50px;
                        width:50px;
                        `
                        rep.innerHTML = "Пилотируемые корабли:"
                        target.replaceWith(rep);
                        rep.after(gif);
                    });
                }
            }
            xhr2.send();
        }
    }
})