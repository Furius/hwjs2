
//Переменные для работы
const createTable = document.createElement("table"),
personP = document.querySelector("#person"),
botP = document.querySelector("#bot"),
butRest = document.querySelector("#buttonRest"),
select = document.querySelector("select");
let countTd = 1;
let personCount = 0;
let botCount = 0;
let checkArr = [];
let letGame = false;
let interval;
let timeout;
let timeout101;

//Сама таблица
document.body.append(createTable);

//Рисуем поле 10 х 10
for(let i = 0; i < 10; i++){
    const createTr = document.createElement("tr");
    createTable.append(createTr);
    for(let j = 0; j < 10; j++){
        const createTd = document.createElement("td");
        createTd.setAttribute("id", `td${countTd}`);
        createTr.append(createTd);
        countTd++;
    }
}

//Функция рандома
function random(min, max){
    let rand = Math.random();
    let rand1 =  (rand * (max - min + 1)) + min;
    let rand2 = Math.floor(rand1);
    return rand2;
}
 
//Вызываем эту функцию если пользователь выбрал легкий уровень сложности
function easy(){
    if(letGame == true){
        interval = setInterval(() => { //В определенном промежутке рисует на рандомных клетках цвет
            if(botP.textContent == "Компьютер: 50"){
                clearInterval(interval);
                alert("YOU LOSE!") //Если у бота 50 очков
            }
            else if(personCount == 50){
                clearInterval(interval);
                alert("YOU WIN!") //Если у клиента 50 очков
            }
            else{
                timeout = setTimeout(() => { //Если клиент не успел нажать
                    if(td.style.backgroundColor != "green"){
                        td.style.backgroundColor = "red";
                        botCount++;
                        botP.innerHTML = `Компьютер: ${botCount}`;
                    }
                }, 1400);
                let tdId = random(1, 100);
                for(let i = 0; i < checkArr.length; i++){ //Если зарандомило число которое уже было(чтобы не рисовало на одной и той самой ячейке)
                    if(checkArr[i] == tdId){
                        tdId = random(1, 100);
                        i = 0;
                    }
                }
                checkArr.push(tdId); //Добавляем в масив число, для дальнейшей проверки
                const td = document.querySelector(`#td${tdId}`); //находим нужную клетку и красим ее в синий
                td.style.backgroundColor = "blue";
            }
        }, 1500);
    }
}

//Все аналогично, только поменялся интервал
function medium(){
    if(letGame == true){
        interval = setInterval(() => {
            if(botP.textContent == "Компьютер: 50"){
                clearInterval(interval);
                alert("YOU LOSE!")
            }
            else if(personCount == 50){
                clearInterval(interval);
                alert("YOU WIN!")
            }
            else{
                timeout = setTimeout(() => {
                    if(td.style.backgroundColor != "green"){
                        td.style.backgroundColor = "red";
                        botCount++;
                        botP.innerHTML = `Компьютер: ${botCount}`;
                    }
                }, 900);
                let tdId = random(1, 100);
                for(let i = 0; i < checkArr.length; i++){
                    if(checkArr[i] == tdId){
                        tdId = random(1, 100);
                        i = 0;
                    }
                }
                checkArr.push(tdId);
                const td = document.querySelector(`#td${tdId}`);
                td.style.backgroundColor = "blue";
            }
        }, 1000);
    }
}

//Все аналогично, только помннялся интервал
function hard(){
    if(letGame == true){
        interval = setInterval(() => {
            if(botP.textContent == "Компьютер: 50"){
                clearInterval(interval);
                alert("YOU LOSE!")
            }
            else if(personCount == 50){
                clearInterval(interval);
                alert("YOU WIN!")
            }
            else{
                timeout = setTimeout(() => {
                    if(td.style.backgroundColor != "green"){
                        td.style.backgroundColor = "red";
                        botCount++;
                        botP.innerHTML = `Компьютер: ${botCount}`;
                    }
                }, 400);
                let tdId = random(1, 100);
                for(let i = 0; i < checkArr.length; i++){
                    if(checkArr[i] == tdId){
                        tdId = random(1, 100);
                        i = 0;
                    }
                }
                checkArr.push(tdId);
                const td = document.querySelector(`#td${tdId}`);
                td.style.backgroundColor = "blue";
            }
        }, 500);
    }
}

//При нажатии на ячейку, если она синяя, красим в зеленый и клиенту +1 очко
createTable.addEventListener("click", function(event){
    if(letGame == true){
        let target = event.target;
        if(target.style.backgroundColor == "blue"){
            personCount++;
            personP.innerHTML = `Игрок: ${personCount}`;
            target.style.backgroundColor = "green";
        }
    }
})

//Кнопка новой игры
butRest.addEventListener("click", function(){
    const clearTd = document.getElementsByTagName("td");
    for(let i = 0; i < clearTd.length; i++){ //Убираем всем клеткам цвет
        clearTd[i].style.backgroundColor = "";
    };
    //Возвращаем все в исходное положение
    botCount = 0;
    personCount = 0;
    personP.innerHTML = "Игрок: 0 ";
    botP.innerHTML = "Компьютер: 0 ";
    letGame = true;
    countTd = 1;
    checkArr = [];
    if(interval != undefined){
        clearInterval(interval);
        clearTimeout(timeout);
        clearTimeout(timeout101);
    }
    //Вызываем функцию которая соответсвует уровню сложности, которой выбрал пользователь
    if(select.value == "easy"){
        easy();
    }
    else if(select.value == "medium"){
        medium();
    }
    else if(select.value == "hard"){
        hard();
    }
})