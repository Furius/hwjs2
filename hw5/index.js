//Записали в переменные все кнопки калькулятора
const tablo = document.querySelector("#tablo"),
button0 = document.querySelector("#but0"),
button1 = document.querySelector("#but1"),
button2 = document.querySelector("#but2"),
button3 = document.querySelector("#but3"),
button4 = document.querySelector("#but4"),
button5 = document.querySelector("#but5"),
button6 = document.querySelector("#but6"),
button7 = document.querySelector("#but7"),
button8 = document.querySelector("#but8"),
button9 = document.querySelector("#but9"),
buttonC = document.querySelector("#butC"),
buttonPlus = document.querySelector("#butplus"),
buttonMinus = document.querySelector("#butminus"),
buttonMulti = document.querySelector("#butmnoj"),
buttonDelit = document.querySelector("#butdelit"),
buttonRavno = document.querySelector("#butravno"),
buttonMminus = document.querySelector("#butmminus"),
buttonMplus = document.querySelector("#butmplus"),
buttonMRC = document.querySelector("#butmrc"),
display = document.querySelector(".display");

let num = [];
let numStr = [];
let numResultStr = [];
let numStrAfterNum = "";
let numStrAfterArr = [];
let ifChangeZnakNumArr = [];
let ifChangeZnakArr = [];

let checkPlus = false;
let checkMult = false;
let checkDelit = false;
let checkMinus = false;

//Функция для суммирования
function plus(arr){
    for(let i = 1; i < arr.length; i++){
        let a = +arr[i - 1]
        let b = +arr[i]
        if(numResultStr.length > 0){
            let c = +numResultStr[0] + b;
            numResultStr = [c];
        }
        numResultStr.push(a + b);
    }
    return numResultStr[0];
};

//Функция для отнимания
function minus(arr){
    for(let i = 1; i < arr.length; i++){
        let a = +arr[i - 1]
        let b = +arr[i]
        if(numResultStr.length > 0){
            let c = +numResultStr[0] - b;
            numResultStr = [c];
        }
        numResultStr.push(a - b);
    }
    return numResultStr[0];
};

//Функция для умножения
function multi(arr){
    for(let i = 1; i < arr.length; i++){
        let a = +arr[i - 1]
        let b = +arr[i]
        if(numResultStr.length > 0){
            let c = +numResultStr[0] * b;
            numResultStr = [c];
        }
        numResultStr.push(a * b);
    }
    return numResultStr[0];
};

//Функция для деления
function delit(arr){
    for(let i = 1; i < arr.length; i++){
        let a = +arr[i - 1]
        let b = +arr[i]
        if(numResultStr.length > 0){
            let c = +numResultStr[0] / b;
            numResultStr = [c];
        }
        numResultStr.push(a / b);
    }
    return numResultStr[0];
};

//Функция для выведения цифры на табло
function buttonFuncNum(but){
    if(num.length > 0){
        if(num[0] == 0){
            num.shift();
            numStr.shift();
        }
    }
    let numOfId = but.id.substr(3, 1);
    let numOfIdFloat = +numOfId;
    num.push(numOfId);
    numStr.push(numOfId);
    tablo.value = num.join("");
}

//Функция очистки табло
function buttonClear(){
    tablo.value = "";
}

//Функция вызываемая при нажатии на плюс, она распределяет цифры которые мы хотим сплюсовать по масивам, а после вызывает саму функцию которая их плюсует
function buttonFuncPlus(){
    if(num[num.length - 1] == "+"){
        return;
    }
    numStr.forEach(element => {
        numStrAfterNum += element;
    });
    if(numStrAfterNum != ""){
        numStrAfterArr.push(numStrAfterNum);
        numStr = [];
        numStrAfterNum = "";
        if(numStrAfterArr.length == 2){
            if(num.indexOf("+") != -1){
                num = [];
                num.push(plus(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [plus(numStrAfterArr)];
                checkPlus = false;
            }
            if(num.indexOf("-") != -1){
                num = [];
                num.push(minus(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [minus(numStrAfterArr)];
                checkMinus = false;
            }
            if(num.indexOf("*") != -1){
                num = [];
                num.push(multi(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [multi(numStrAfterArr)];
                checkMult = false;
            }
            if(num.indexOf(":") != -1){
                num = [];
                num.push(delit(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [delit(numStrAfterArr)];
                checkDelit = false;
            }
        }
    }
    //Если мы нажали плюс, а на табло пусто, ничего не произойдет
    if(tablo.value == ""){
        return;
    }
    num.push("+");
    tablo.value = num.join("");
    checkPlus = true;
}

//Аналогично, только для минуса
function buttonFuncMinus(){
    if(num[num.length - 1] == "-"){
        return;
    }
    numStr.forEach(element => {
        numStrAfterNum += element;
    });
    if(numStrAfterNum != ""){
        numStrAfterArr.push(numStrAfterNum);
        numStr = [];
        numStrAfterNum = "";
        if(numStrAfterArr.length == 2){
            if(num.indexOf("+") != -1){
                num = [];
                num.push(plus(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [plus(numStrAfterArr)];
                checkPlus = false;
            }
            if(num.indexOf("-") != -1){
                num = [];
                num.push(minus(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [minus(numStrAfterArr)];
                checkMinus = false;
            }
            if(num.indexOf("*") != -1){
                num = [];
                num.push(multi(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [multi(numStrAfterArr)];
                checkMult = false;
            }
            if(num.indexOf(":") != -1){
                num = [];
                num.push(delit(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [delit(numStrAfterArr)];
                checkDelit = false;
            }
        }
    }
    if(tablo.value == ""){
        return;
    }
    num.push("-");
    tablo.value = num.join("");
    checkMinus = true;
}

//Аналогично, только для умножения
function buttonFuncMulti(){
    if(num[num.length - 1] == "*"){
        return;
    }
    numStr.forEach(element => {
        numStrAfterNum += element;
    });
    if(numStrAfterNum != ""){
        numStrAfterArr.push(numStrAfterNum);
        numStr = [];
        numStrAfterNum = "";
        if(numStrAfterArr.length == 2){
            if(num.indexOf("+") != -1){
                num = [];
                num.push(plus(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [plus(numStrAfterArr)];
                checkPlus = false;
            }
            if(num.indexOf("-") != -1){
                num = [];
                num.push(minus(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [minus(numStrAfterArr)];
                checkMinus = false;
            }
            if(num.indexOf("*") != -1){
                num = [];
                num.push(multi(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [multi(numStrAfterArr)];
                checkMult = false;
            }
            if(num.indexOf(":") != -1){
                num = [];
                num.push(delit(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [delit(numStrAfterArr)];
                checkDelit = false;
            }
        }
    }
    if(tablo.value == ""){
        return;
    }
    num.push("*");
    tablo.value = num.join("");
    checkMult = true;
}

//Аналогично, только для деления
function buttonFuncDelit(){
    if(num[num.length - 1] == ":"){
        return;
    }
    numStr.forEach(element => {
        numStrAfterNum += element;
    });
    if(numStrAfterNum != ""){
        numStrAfterArr.push(numStrAfterNum);
        numStr = [];
        numStrAfterNum = "";
        if(numStrAfterArr.length == 2){
            if(num.indexOf("+") != -1){
                num = [];
                num.push(plus(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [plus(numStrAfterArr)];
                checkPlus = false;
            }
            if(num.indexOf("-") != -1){
                num = [];
                num.push(minus(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [minus(numStrAfterArr)];
                checkMinus = false;
            }
            if(num.indexOf("*") != -1){
                num = [];
                num.push(multi(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [multi(numStrAfterArr)];
                checkMult = false;
            }
            if(num.indexOf(":") != -1){
                num = [];
                num.push(delit(numStrAfterArr))
                numResultStr = [];
                numStrAfterArr = [delit(numStrAfterArr)];
                checkDelit = false;
            }
        }
    }
    if(tablo.value == ""){
        return;
    }
    num.push(":");
    tablo.value = num.join("");
    checkDelit = true;
}

//События нажатия на клавиши
button0.onclick = function(){
    buttonFuncNum(button0);
}
button1.onclick = function(){
    buttonFuncNum(button1);
}
button2.onclick = function(){
    buttonFuncNum(button2);
}
button3.onclick = function(){
    buttonFuncNum(button3);
}
button4.onclick = function(){
    buttonFuncNum(button4);
}
button5.onclick = function(){
    buttonFuncNum(button5);
}
button6.onclick = function(){
    buttonFuncNum(button6);
}
button7.onclick = function(){
    buttonFuncNum(button7);
}
button8.onclick = function(){
    buttonFuncNum(button8);
}
button9.onclick = function(){
    buttonFuncNum(button9);
}

//При нажатии на клавишу очистки
buttonC.onclick = function(){
    buttonClear();
    num = [];
    numStr = [];
    numStrAfterNum = "";
    numStrAfterArr = [];
    ifChangeZnakNumArr = [];
    ifChangeZnakArr = [];

    checkPlus = false;
    checkMult = false;
    checkDelit = false;
    checkMinus = false;
}

//Нажатие на плюс, если мы написали 0, а после нажали плюс, то этот 0 сотреться со всех масивов и плюс не напишеться
buttonPlus.onclick = function(){
    if(num.length > 0){
        if(num[0] == 0){
            num.shift();
            numStr.shift();
            tablo.value = "";
            return;
        }
    }
    if(num.length >= 1){
        buttonFuncPlus();
    }
}

////Аналогично, только для минуса
buttonMinus.onclick = function(){
    if(num.length > 0){
        if(num[0] == 0){
            num.shift();
            numStr.shift();
            tablo.value = "";
            return;
        }
    }
    if(num.length >= 1){
        buttonFuncMinus();
    }
}

//Аналогично, только для умножения
buttonMulti.onclick = function(){
    if(num.length > 0){
        if(num[0] == 0){
            num.shift();
            numStr.shift();
            tablo.value = "";
            return;
        }
    }
    if(num.length >= 1){
        buttonFuncMulti();
    }
}

//Аналогично, только для деления
buttonDelit.onclick = function(){
    if(num.length > 0){
        if(num[0] == 0){
            num.shift();
            numStr.shift();
            tablo.value = "";
            return;
        }
    }
    if(num.length >= 1){
        buttonFuncDelit();
    }
}

//Событие при нажатии на равно
buttonRavno.onclick = function(){
    //Проверяет какая клавиша была нажата и в соответствии с ней вызывает нужную функцию, распределяя цифры по масивам, которые после будут обработаны в функциях для знаков
    if(checkPlus == true){
        numStr.forEach(element => {
            numStrAfterNum += element;
        });
        if(numStrAfterNum != ""){
            numStrAfterArr.push(numStrAfterNum);
            numStr = [];
            numStrAfterNum = "";
        }
        if(numStrAfterArr.length >= 2){
            tablo.value = plus(numStrAfterArr);
            num = [tablo.value];
            numStr.push(tablo.value);
            numResultStr = [];
            numStrAfterArr = [];
            checkPlus = false;
        }
    }
    if(checkMinus == true){
        numStr.forEach(element => {
            numStrAfterNum += element;
        });
        if(numStrAfterNum != ""){
            numStrAfterArr.push(numStrAfterNum);
            numStr = [];
            numStrAfterNum = "";
        }
        if(numStrAfterArr.length >= 2){
            tablo.value = minus(numStrAfterArr);
            num = [tablo.value];
            numStr.push(tablo.value);
            numResultStr = [];
            numStrAfterArr = [];
            checkMinus = false;
        }
    }
    if(checkMult == true){
        numStr.forEach(element => {
            numStrAfterNum += element;
        });
        if(numStrAfterNum != ""){
            numStrAfterArr.push(numStrAfterNum);
            numStr = [];
            numStrAfterNum = "";
        }
        if(numStrAfterArr.length >= 2){
            tablo.value = multi(numStrAfterArr);
            num = [tablo.value];
            numStr.push(tablo.value);
            numResultStr = [];
            numStrAfterArr = [];
            checkMult = false;
        }
    }
    if(checkDelit == true){
        numStr.forEach(element => {
            numStrAfterNum += element;
        });
        if(numStrAfterNum != ""){
            numStrAfterArr.push(numStrAfterNum);
            numStr = [];
            numStrAfterNum = "";
        }
        if(numStrAfterArr.length >= 2){
            tablo.value = delit(numStrAfterArr);
            num = [tablo.value];
            numStr.push(tablo.value);
            numResultStr = [];
            numStrAfterArr = [];
            checkDelit = false;
        }
    }
}

let memory;
let createImg;

//Событие при нажатии на кнопку памяти
buttonMminus.onclick = function(){
    if(tablo.value != ""){
        memory = tablo.value;
        //Если на табло был какой-либо знак, ничего не произойдет
        if(memory.indexOf("+") != -1){
            return;
        }
        if(memory.indexOf("-") != -1){
            return;
        }
        if(memory.indexOf(":") != -1){
            return;
        }
        if(memory.indexOf("*") != -1){
            return;
        }
        //Иначе число запишется в память и появиться значек М
        else{
            if(createImg != display.firstChild){
                createImg = document.createElement("img");
                createImg.setAttribute("src", "m.png")
                createImg.style.cssText = `
                width: 10px;
                height: 10px;
                position: absolute;
                z-index: 100;
                margin-top: 10px;
                margin-left: 10px;
                `;
                tablo.before(createImg);
            }
        }
    }
}

//Аналогично для клавишы памяти м+
buttonMplus.onclick = function(){
    if(tablo.value != ""){
        memory = tablo.value;
        if(memory.indexOf("+") != -1){
            return;
        }
        if(memory.indexOf("-") != -1){
            return;
        }
        if(memory.indexOf(":") != -1){
            return;
        }
        if(memory.indexOf("*") != -1){
            return;
        }
        else{
            if(createImg == undefined){
                createImg = document.createElement("img");
                createImg.setAttribute("src", "m.png")
                createImg.style.cssText = `
                width: 10px;
                height: 10px;
                position: absolute;
                z-index: 100;
                margin-top: 10px;
                margin-left: 10px;
                `;
                tablo.before(createImg);
            }
        }
    }
}

//При нажатии на клавишу MRC
buttonMRC.onclick = function(){
    //Если на табло находиться число которое записано в памяти, память очиститься, а значек М пропадет
    if(tablo.value == memory){
        memory = undefined;
        createImg.remove();
    }
    //Иначе на табло будет записано число которое хранится в памяти, а так же будут анулированы все масивы и все переменные будут возвращены к изначальным значениям, а в масивы с которыми работают функции знаков будет записано число которое хранилось в памяти, таким образом с этим числом будет возможно проводить различные математические операции.
    if(memory != undefined){
        tablo.value = memory;
        num = [];
        numStr = [];
        numStrAfterNum = "";
        numStrAfterArr = [];
        ifChangeZnakNumArr = [];
        ifChangeZnakArr = [];

        checkPlus = false;
        checkMult = false;
        checkDelit = false;
        checkMinus = false;
        num.push(memory);
        numStr.push(memory);
    }
}