//ДЛЯ КРАСИВОГО ОТОБРАЖЕНИЯ СТРАНИЦЫ, ВОЗМОЖНО НУЖНО БУДЕТ УМЕНЬШИТЬ ИЛИ УВЕЛИЧИТЬ РАСШИРЕНИЕ БРАУЗЕРА

const divKorj = document.querySelector(".table"),
sousClassic = document.querySelector("#sousClasic"),
sousBarb = document.querySelector("#sousBarb"),
sousRikota = document.querySelector("#sousRikota"),
mazarela1 = document.querySelector("#mazarela1"),
mazarela2 = document.querySelector("#mazarela2"),
mazarela3 = document.querySelector("#mazarela3"),
telya = document.querySelector("#telya"),
vetchina1 = document.querySelector("#vetchina1"),
vetchina2 = document.querySelector("#vetchina2"),
divsousClassic = document.querySelector("#divsousClasic"),
divsousBarb = document.querySelector("#divsousBarb"),
divsousRikota = document.querySelector("#divsousRikota"),
divmazarela1 = document.querySelector("#divmazarela1"),
divmazarela2 = document.querySelector("#divmazarela2"),
divmazarela3 = document.querySelector("#divmazarela3"),
divtelya = document.querySelector("#divtelya"),
divvetchina1 = document.querySelector("#divvetchina1"),
divvetchina2 = document.querySelector("#divvetchina2"),
pricediv = document.querySelector(".price"),
pricep = pricediv.getElementsByTagName("p")[0],
skidkadiv = document.querySelector(".skidka"),
skidkap = pricediv.getElementsByTagName("a")[0];

let price = 40;
let topKorj = -802;
let topLittleKorj = -565;
let hwKorj = 80;
let checkOnhwKorj = false;
let checkKorj = true;
let wrongData = false;

const divwrapper = document.querySelector(".table-wrapper");
/*
//Реализация через события мышки

sousClassic.addEventListener("mousedown", function (event) {
    let shiftX = event.clientX - sousClassic.getBoundingClientRect().left;
    let shiftY = event.clientY - sousClassic.getBoundingClientRect().top;
    //Вычитаем координаты от левой стороны и верхней стороны документа, координаты на которых находиться картинка чтобы получить координаты нашего клика в соотношении уже не координат документа, а левой и верхней стороны самой картинки

    sousClassic.style.position = "absolute";
    sousClassic.style.width = "1000px";
    sousClassic.style.height = "700px";
    sousClassic.style.zIndex = 1000; //Разместить поверх всех элементов

    moveAt(event.pageX, event.pageY);

    function moveAt(pageX, pageY){
        sousClassic.style.left = pageX - shiftX + "px";
        sousClassic.style.top = pageY - shiftY + "px";
        //Узнаем координаты нашего клика путем вычитания от координат страницы, координат нашего клика в отношении картинки
    }
    //Функция для сдвига картинки на место где произошел клик

    function mouseMove(event){
        moveAt(event.pageX, event.pageY); //При передвижении мышки каждый раз будет вызыватся функция moveAt, которая будет менять координаты картинки на те координаты на которых находиться курсор(event.pageX, event.pageY)
    }
    
    document.addEventListener("mousemove", mouseMove);
    //Отслеживаем на документе, потому что при быстром перемещении мышки, курсор может слететь с мяча

    sousClassic.addEventListener("mouseup", function(){
        document.removeEventListener("mousemove", mouseMove);
    })
})
*/

//Для начала переноса ингридиентов
sousClassic.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})
sousBarb.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})
sousRikota.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})
mazarela1.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})
mazarela2.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})
mazarela3.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})
telya.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})
vetchina1.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})
vetchina2.addEventListener("dragstart", function(event){
    event.dataTransfer.effectAllowed = "move"; //Говорим браузеру что это перетаскиваемый элемент
    event.dataTransfer.setData("Text", this.id);
})

divKorj.addEventListener("dragover", function (evt) {
    // отменяем стандартное обработчик события dragover.
    // реализация данного обработчика по умолчанию не позволяет перетаскивать данные на целевой элемент, так как большая
    // часть веб страницы не может принимать данные.
    // Для того что бы элемент мог принять перетаскиваемые данные необходимо отменить стандартный обработчик.
    if (evt.preventDefault) evt.preventDefault();
    return false;
}, false);

//Когда отпускаем ингридиент на корж
divKorj.addEventListener("drop", function (event) {
    if (event.preventDefault) event.preventDefault();
    if (event.stopPropagation) event.stopPropagation();
    let id = event.dataTransfer.getData("Text");
    const getElementFromData = document.querySelector(`#${id}`);
    getElementFromData.style.height = "100%";
    if(this.firstElementChild != null){ 
        if(hwKorj == 100 && checkOnhwKorj == true){ //Если корж маленький
            this.firstElementChild.style.position = "relative";
            this.firstElementChild.style.bottom = "-77px";
            getElementFromData.style.cssText = `
            position:relative;
            height:80%;
            width:80%;
            top:${topLittleKorj}px;
            `
            topLittleKorj += -639;
        }
        else{ //Если корж большой
            getElementFromData.style.cssText = `
            height:100%;
            position: relative;
            top: ${topKorj}px;
            `
            topKorj += -802;
        }
    }
    if(hwKorj == 100 && this.firstElementChild == null && checkOnhwKorj == true){ //Если мы дропаем первый элемент и корж маленький
        getElementFromData.style.cssText = `
        height:80%;
        width:80%;
        `
    }
    checkKorj = false;
    this.append(getElementFromData);
})

//Для цены
divKorj.addEventListener("drop", function(event){
    let id = event.dataTransfer.getData("Text");
    if(id == "sousClasic"){
        price += 7;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(id == "sousBarb"){
        price += 10;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(id == "sousRikota"){
        price += 8;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(id == "mazarela1"){
        price += 15;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(id == "mazarela2"){
        price += 20;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(id == "mazarela3"){
        price += 25;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(id == "telya"){
        price += 17;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(id == "vetchina1"){
        price += 18;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(id == "vetchina2"){
        price += 20;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
})

//Для изменения размера коржа, а соответственно и цены за 1 корж
divKorj.addEventListener("click", function(event){
    if(checkKorj == true){
        if(hwKorj == 80){
            this.style.cssText = `
            background-size: ${hwKorj}% ${hwKorj}%;
            background-position: 95px 77px;
            `
            hwKorj += 20;
            checkOnhwKorj = true;
            if(price == 40){
                price = 30;
                pricep.innerHTML = `Ціна: ${price} грн`;
            }
        }
        else if(hwKorj == 100){
            this.style.cssText = `
            background-size: ${hwKorj}% ${hwKorj}%;
            background-position: ;
            `
            hwKorj -= 20;
            checkOnhwKorj = false;
            if(price == 30){
                price = 40;
                pricep.innerHTML = `Ціна: ${price} грн`;
            }
        }
    }
});

//Для удаления ингридиента с коржа и возврат этого ингридиента обратно в его див.
//При удалении ингридиента, цена будет уменьшаться
divKorj.addEventListener("click", function(event){
    let target = event.target;
    let targetId = target.id;
    if(target.className != "table"){
        let getDivOfTarget = "div" + targetId;
        const divOfTarget = document.querySelector(`#${getDivOfTarget}`)
        target.style.cssText = `
        height:100px;
        width:100%;
        `
        if(hwKorj == 80){
            if(this.firstElementChild.nextSibling != null){
                topKorj -= -802;
            }
        }
        if(hwKorj == 100){
            if(this.firstElementChild.nextSibling != null){
                topLittleKorj -= -639;
            }
            let nextsib = this.firstElementChild.nextSibling;
            if(nextsib != null){
                if(nextsib.nextSibling == null){
                    this.firstElementChild.style.bottom = "4px";
                }
            }
        }
        divOfTarget.append(target);
        
    }
    if(divKorj.firstElementChild == null){
        checkKorj = true;
    }
    if(targetId == "sousClasic"){
        price -= 7;
        pricep.innerHTML = `Ціна: ${price} грн`;
    }
    if(targetId == "sousBarb"){
        price -= 10;
        pricep.innerHTML = `Ціна: ${price} грн`;
    }
    if(targetId == "sousRikota"){
        price -= 8;
        pricep.innerHTML = `Ціна: ${price} грн`;
    }
    if(targetId == "mazarela1"){
        price -= 15;
        pricep.innerHTML = `Ціна: ${price} грн`;
    }
    if(targetId == "mazarela2"){
        price -= 20;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(targetId == "mazarela3"){
        price -= 25;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(targetId == "telya"){
        price -= 17;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(targetId == "vetchina1"){
        price -= 18;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
    if(targetId == "vetchina2"){
        price -= 20;
        pricep.innerHTML = `Ціна: ${price} грн`
    }
});

skidkadiv.addEventListener("mouseenter", function(event){
    let rand1 = Math.floor(Math.random() * -1700);
    let rand2 = Math.floor(Math.random() * 800);
    skidkadiv.style.position = "relative";
    skidkadiv.style.zIndex = 1000;
    skidkadiv.style.left = rand1 + "px";
    skidkadiv.style.top = rand2 + "px";
})

//Проверка на верность ввода
const patternName = /^[а-яА-Я]+ [а-яА-Я]+$/,
patternPhone = /^\+38\d{10}$/,
patternEmail = /\b[a-z0-9._]+@[a-z0-9.-]+\.[a-z]{2,4}\b/i;

//При потере фокуса инпут будет проверятся на верность ввода 
form1[0].addEventListener("blur", function(event){
    if(form1[0].value != ""){
        let value = form1[0].value;
        if(!patternName.test(value)){
            form1[0].style.backgroundColor = "red";
        }
        else{
            form1[0].style.backgroundColor = "green";
        }
    }
})
form1[1].addEventListener("blur", function(event){
    if(form1[1].value != ""){
        let value = form1[1].value;
        if(!patternPhone.test(value)){
            form1[1].style.backgroundColor = "red";
        }
        else{
            form1[1].style.backgroundColor = "green";
        }
    }
})
form1[2].addEventListener("blur", function(event){
    if(form1[2].value != ""){
        let value = form1[2].value;
        if(!patternEmail.test(value)){
            form1[2].style.backgroundColor = "red";
        }
        else{
            form1[2].style.backgroundColor = "green";
        }
    }
})

//При нажатии на "зкинути", все инпуты будут сброшены
form1[3].addEventListener("click", function(event){
    for(let i = 0; i <= 2; i++){
        form1[i].style.backgroundColor = "";
    }
    wrongData = true;
})

//При нажатии на отправку заказа, вам на почту придет вся информация которую ввел пользователь(так же повторно проверяется верность инпутов)
form1[4].addEventListener("click", function(event){
    for(let i = 0; i <= 2; i++){
        if(form1[i].value != ""){
            let value = form1[i].value;
            if(i == 0 && !patternName.test(value)){
                alert("Ви ввели невірні данні");
                wrongData = true;
                break;
            }
            if(i == 1 && !patternPhone.test(value)){
                alert("Ви ввели невірні данні");
                wrongData = true;
                break;
            }
            if(i == 2 && !patternEmail.test(value)){
                alert("Ви ввели невірні данні");
                wrongData = true;
                break;
            }
        }
        else{
            wrongData = true;
            alert("Ви нічого не ввели")
            break;
        }
        wrongData = false;
    }
    if(wrongData == false){
        let name = form1[0].value;
        let phone = form1[1].value;
        let email = form1[2].value;
        let sendPrice = price + "грн";
        let pizza;
        if(checkOnhwKorj == true){
            pizza = "маленька";
        }
        else{
            pizza = "велика";
        }
        Email.send({
            SecureToken : "c8931f26-85b6-4ee6-9e68-f46885620d4f",
            To : 'philipsevene@gmail.com', //ПИСЬМО ПРИДЕТ В СПАМ
            From : "dsova2004@gmail.com",
            Subject : "Піцерія Папа Карло",
            Body : `Замовник: ${name};
            Телефон: ${phone};
            Електронна адреса: ${email};
            Ціна: ${sendPrice};
            Розмір: ${pizza}`
        }).then(
            message => alert("Дякуємо що обираєте нас :)")
        );
    }
})
//dsova2004@gmail.com
//C8901820E5F9066B1B35E9947A313535208D
//smtp.elasticemail.com
//2525