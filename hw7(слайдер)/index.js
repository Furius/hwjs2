﻿//Переменные для работы

let countImg = 2, //Счетчик картинок
letDraw = true, //Для конца анимации
getImg; 

//Все кнопки и картинки
const divImg = document.querySelector("#divImg"),
next = document.querySelector("#next"),
prev = document.querySelector("#prev"),
img1 = document.querySelector("#img1"),
img2 = document.querySelector("#img2"),
img3 = document.querySelector("#img3"),
img4 = document.querySelector("#img4"),
img5 = document.querySelector("#img5"),
img6 = document.querySelector("#img6");

//При клике на кнопку Next
next.addEventListener("click", function(event){
   if(letDraw == true){ //Если закончилась предыдущая анимация
        divImg.childNodes.forEach(element => { //Находим элемент на котором в данный момент остановились
            if(element.nodeType != 3){
                let styles = getComputedStyle(element);
                if(styles.width == "500px"){
                    getImg = element;
                    return;
                }
            }
        });
        if(countImg > 6){ //Если это последняя картинка, рисуем первую
            countImg = 2;
            getImg.style.width = "0px";
            divImg.firstElementChild.style.width = "500px";
        }
        else{ //Иначе, рисуем следующую картинку
            getImg.style.width = "0px"
            getImg.nextElementSibling.style.width = "500px";
            getImg.nextElementSibling.style.backgroundImage = `url("img/пейзаж${countImg}.jpg")`;
            countImg++;
            letDraw = false;
        }
    }
    if(getImg != undefined){ //Для того чтобы не начиналась новая анимация до окончания предыдущей
        getImg.addEventListener("transitionend", function(){
            letDraw = true;
        });
    }
})

//При нажатии на кнопку Previous
prev.addEventListener("click", function(event){
    if(letDraw == true){ //Аналогично
        divImg.childNodes.forEach(element => {
            if(element.nodeType != 3){
                let styles = getComputedStyle(element);
                if(styles.width == "500px"){
                    getImg = element;
                    return;
                }
            }
        });
        if(countImg == 2){ //Если картинка первая - рисуем последнюю
            countImg = 7;
            getImg.style.width = "0px";
            divImg.lastElementChild.style.width = "500px";
            divImg.lastElementChild.style.backgroundImage = `url("img/пейзаж${countImg - 1}.jpg")`;
        }
        else{ //Если нет, предыдущую
            getImg.style.width = "0px"
            getImg.previousElementSibling.style.width = "500px";
            getImg.previousElementSibling.style.backgroundImage = `url("img/пейзаж${countImg - 2}.jpg")`;
            countImg--;
            letDraw = false;
        }
    }
    if(getImg != undefined){ //Аналогично
        getImg.addEventListener("transitionend", function(){
            letDraw = true;
        });
    }
})